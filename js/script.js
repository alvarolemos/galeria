$(document).ready(function() {

  // URL dos JSONs com informacoes dos albums e fotos
  var albumsUrl = "http://jsonplaceholder.typicode.com/albums";
  var photosUrl = "http://jsonplaceholder.typicode.com/photos";

  // Faz requisicao de todas as fotos
  var photos = [];
  $.get(photosUrl, function(data) {
    photos = data;
  });

  // Funcao responsavel por exibir as thumbnails de um album, dado o seu ID
  function showThumbnails(albumId, albumTitle) {

    var albumPhotos = photos.filter(function(photo) {
      return photo.albumId == albumId;
    });

    // Filtra as 5 primeiras fotos do botao clicado
    albumPhotos = albumPhotos.slice(0, 5);

    // Exibe o titulo do album
    $(".thumbnails").closest(".container").find("h1").text("Fotos do álbum " + albumTitle);

    // Limpa thumbnails atualmente em exibicao e exibe as 5 primeiras
    // thumbnails do album selecionado
    $(".thumbnails").empty();
    albumPhotos.forEach(function(photo) {
      var auxImg = $("<img>");
      auxImg.data("id", photo.id);
      auxImg.attr("src", photo.thumbnailUrl);
      $(".thumbnails").append(auxImg);
    });

    // Exibe a primeira foto do album
    showFullPhoto(albumPhotos[0].id);

  }

  // Funcao responsavel por exibir uma imagem, dado seu ID
  function showFullPhoto(photoId) {

    // Filtra a foto referente ao botao selecionado
    var fullPhoto = photos.filter(function(photo) {
      return photo.id == photoId;
    });

    // Exibe titulo da foto
    $(".full-photo").closest(".container").find("h2").text(fullPhoto[0].title);


    // Limpa foto atualmente em exibicao e exibe a referente ao thumbnail
    // selecionado
    var auxImg = $("<img>");
    auxImg.attr("src", fullPhoto[0].url);
    $(".full-photo").empty().append(auxImg);

  }

  // Faz requisicao dos 10 primeiros albuns
  $.get(albumsUrl, function(data) {

    // Filtra os 10 primeiros albuns
    var albums = data.slice(0, 10);

    // Cria um botao para cada album
    albums.forEach(function(album) {
      var auxBtn = $("<button></button>");
      auxBtn.addClass("btn btn-default");
      auxBtn.data("id", album.id);
      auxBtn.data("title", album.title);
      $(".albums").append(auxBtn.text(album.title));
    });

    // Exibe o primeiro album
    showThumbnails(albums[0].id, albums[0].name);

  });

  // Exibe os 5 primeiros thumbnails de um album, quando ele eh selecionado
  $(".albums").on("click", "button", function() {
    showThumbnails($(this).data("id"), $(this).data("title"));
  });

  // Exibe a foto referente a um thumbnail, quando ele eh selecionado
  $(".thumbnails").on("click", "img", function() {
    showFullPhoto($(this).data("id"));
  });

});
